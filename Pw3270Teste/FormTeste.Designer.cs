﻿namespace Pw3270Teste
{
    partial class FormTeste
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPw = new System.Windows.Forms.RichTextBox();
            this.btnInit = new System.Windows.Forms.Button();
            this.txtSessao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGetScreen = new System.Windows.Forms.Button();
            this.btnGetStatus = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnSplitScreen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPw
            // 
            this.txtPw.Location = new System.Drawing.Point(20, 20);
            this.txtPw.Name = "txtPw";
            this.txtPw.Size = new System.Drawing.Size(610, 474);
            this.txtPw.TabIndex = 0;
            this.txtPw.Text = "";
            // 
            // btnInit
            // 
            this.btnInit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInit.Location = new System.Drawing.Point(640, 124);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(115, 23);
            this.btnInit.TabIndex = 5;
            this.btnInit.Text = "Init";
            this.btnInit.UseVisualStyleBackColor = true;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // txtSessao
            // 
            this.txtSessao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSessao.Location = new System.Drawing.Point(640, 39);
            this.txtSessao.Name = "txtSessao";
            this.txtSessao.Size = new System.Drawing.Size(115, 20);
            this.txtSessao.TabIndex = 6;
            this.txtSessao.Text = "pw3270:A";
            this.txtSessao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(653, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nome da Sessão";
            // 
            // btnGetScreen
            // 
            this.btnGetScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetScreen.BackColor = System.Drawing.Color.Crimson;
            this.btnGetScreen.ForeColor = System.Drawing.Color.White;
            this.btnGetScreen.Location = new System.Drawing.Point(640, 182);
            this.btnGetScreen.Name = "btnGetScreen";
            this.btnGetScreen.Size = new System.Drawing.Size(115, 23);
            this.btnGetScreen.TabIndex = 5;
            this.btnGetScreen.Text = "GetScreen";
            this.btnGetScreen.UseVisualStyleBackColor = false;
            this.btnGetScreen.Click += new System.EventHandler(this.btnGetScreen_Click);
            // 
            // btnGetStatus
            // 
            this.btnGetStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetStatus.Location = new System.Drawing.Point(640, 211);
            this.btnGetStatus.Name = "btnGetStatus";
            this.btnGetStatus.Size = new System.Drawing.Size(115, 23);
            this.btnGetStatus.TabIndex = 5;
            this.btnGetStatus.Text = "GetStatus";
            this.btnGetStatus.UseVisualStyleBackColor = true;
            this.btnGetStatus.Click += new System.EventHandler(this.btnGetStatus_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(676, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Servidor";
            // 
            // txtServer
            // 
            this.txtServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServer.Location = new System.Drawing.Point(640, 78);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(115, 20);
            this.txtServer.TabIndex = 8;
            this.txtServer.Text = "zos.efglobe.com";
            this.txtServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnConnect
            // 
            this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnect.Location = new System.Drawing.Point(640, 153);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(115, 23);
            this.btnConnect.TabIndex = 10;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnSplitScreen
            // 
            this.btnSplitScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSplitScreen.Location = new System.Drawing.Point(640, 240);
            this.btnSplitScreen.Name = "btnSplitScreen";
            this.btnSplitScreen.Size = new System.Drawing.Size(115, 23);
            this.btnSplitScreen.TabIndex = 11;
            this.btnSplitScreen.Text = "SplitScreen";
            this.btnSplitScreen.UseVisualStyleBackColor = true;
            this.btnSplitScreen.Click += new System.EventHandler(this.btnSplitScreen_Click);
            // 
            // FormTeste
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 506);
            this.Controls.Add(this.btnSplitScreen);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtServer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSessao);
            this.Controls.Add(this.btnGetStatus);
            this.Controls.Add(this.btnGetScreen);
            this.Controls.Add(this.btnInit);
            this.Controls.Add(this.txtPw);
            this.Name = "FormTeste";
            this.Text = "Pw3270 Tests";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtPw;
        private System.Windows.Forms.Button btnInit;
        private System.Windows.Forms.TextBox txtSessao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGetScreen;
        private System.Windows.Forms.Button btnGetStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSplitScreen;
    }
}

