﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Pw3270Teste
{
    public class SessionPw
    {
        private string _sessionName;
        private string _hostname;
        private IntPtr _hSession;

        public SessionPw(string sessionName)
        {
            _sessionName = sessionName;
        }

        public void Init()
        {
            _hSession = tn3270_create_session(_sessionName);
            tn3270_set_unlock_delay(_hSession, 100);
            tn3270_set_charset(_hSession, "UTF-8");
        }

        public void Dispose()
        {
            tn3270_destroy_session(_hSession);
        }

        public void EraseInput()
        {
            tn3270_erase_input(_hSession);
        }

        public int Action(string action)
        {
            return tn3270_action(_hSession, action);
        }

        public string GetVersion()
        {
            StringBuilder str = new StringBuilder(10);
            tn3270_get_version(_hSession, str, 10);
            return str.ToString();
        }

        public string GetRevision()
        {
            StringBuilder str = new StringBuilder(10);
            tn3270_get_revision(_hSession, str, 10);
            return str.ToString();
        }

        public int Connect(string hostname, int wait = 60)
        {
            return tn3270_connect(_hSession, hostname, wait);
        }

        public int GetState()
        {
            return tn3270_get_cstate(_hSession);
        }

        public bool IsConnected()
        {
            return tn3270_is_connected(_hSession) != 0;
        }

        public int IsReady()
        {
            return tn3270_is_ready(_hSession);
        }

        public int Disconnect()
        {
            return tn3270_disconnect(_hSession);
        }

        public virtual void WaitForReady(int seconds = 60)
        {
            tn3270_wait_for_ready(_hSession, seconds);
        }

        public virtual int Wait(int seconds)
        {
            return tn3270_wait(_hSession, seconds);
        }

        public string GetString(int addr, int strlen)
        {
            StringBuilder str = new StringBuilder(strlen + 1);
            tn3270_get_string(_hSession, addr, str, strlen);
            return str.ToString();
        }

        public virtual string GetScreenAt(int row, int col, int strlen)
        {
            StringBuilder str = new StringBuilder(strlen + 1);
            tn3270_get_string_at(_hSession, row, col, str, strlen);
            return str.ToString(0, strlen);
        }

        public int SetTextAt(int row, int col, string str)
        {
            return tn3270_set_string_at(_hSession, row, col, str);
        }

        public int SetStringAt(int row, int colInicial, int colFinal, string str)
        {
            var strlen = (colFinal - colInicial) + 1;

            if (str == null || string.IsNullOrWhiteSpace(str))
                str = string.Empty;

            if (str.Length < strlen)
                str = str.PadRight(strlen);

            return tn3270_set_string_at(_hSession, row, colInicial, str);
        }

        public int WaitForStringAt(int row, int col, string str, int timeout)
        {
            return tn3270_wait_for_string_at(_hSession, row, col, str, timeout);
        }

        public int CmpStringAt(int row, int col, string str)
        {
            return tn3270_cmp_string_at(_hSession, row, col, str);
        }

        public void SetCursorPosition(int row, int col)
        {
            tn3270_set_cursor_position(_hSession, row, col);
        }

        public void SetCursorPosition(int addr)
        {
            tn3270_set_cursor_addr(_hSession, addr - 1);
        }

        public int GetCursor()
        {
            return tn3270_get_cursor_addr(_hSession) + 1;

        }

        public int SetUnlockDelay(int ms)
        {
            return tn3270_set_unlock_delay(_hSession, ms);
        }

        public virtual void Enter()
        {
            tn3270_enter(_hSession);
            WaitForReady();
        }

        public virtual void PfKey(int key)
        {
            tn3270_pfkey(_hSession, key);
            WaitForReady();
        }

        public virtual void PaKey(int key)
        {
            tn3270_pakey(_hSession, key);
            WaitForReady();
        }

        public int GetWidth()
        {
            return tn3270_get_width(_hSession);
        }

        public int GetHeight()
        {
            return tn3270_get_height(_hSession);
        }

        public int GetLength()
        {
            return tn3270_get_length(_hSession);
        }

        #region DllImport

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern IntPtr tn3270_create_session(string name);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_destroy_session(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_version(IntPtr session, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_revision(IntPtr session, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_connect(IntPtr session, string host, int wait);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_is_connected(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_is_ready(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_disconnect(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_wait_for_ready(IntPtr session, int seconds);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_wait(IntPtr session, int seconds);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_cstate(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_program_message(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_secure(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_contents(IntPtr session, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_string(IntPtr session, int addr, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_string_at(IntPtr session, int row, int col, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_string_at(IntPtr session, int row, int col, string str);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_wait_for_string_at(IntPtr session, int row, int col, string key, int timeout);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_cmp_string_at(IntPtr session, int row, int col, string str);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_unlock_delay(IntPtr session, int ms);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_cursor_position(IntPtr session, int row, int col);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_cursor_addr(IntPtr session, int addr);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_cursor_addr(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_enter(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_pfkey(IntPtr session, int key);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_pakey(IntPtr session, int key);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_width(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_height(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_length(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_charset(IntPtr session, string str);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_url(IntPtr session, string str);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_url(IntPtr session, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_set_error_message(IntPtr session, string str);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_get_error_message(IntPtr session, StringBuilder str, int strlen);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_erase_input(IntPtr session);

        [DllImport("lib3270-mono", CallingConvention = CallingConvention.Cdecl)]
        internal static extern int tn3270_action(IntPtr session, string action);

        #endregion
    }
}
