﻿using System;
using System.Windows.Forms;

namespace Pw3270Teste
{
    public partial class FormTeste : Form
    {
        private SessionPw _sessionPw;
        
        public FormTeste()
        {
            InitializeComponent();
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            try
            {
                _sessionPw = new SessionPw(txtSessao.Text);
                _sessionPw.Init();
                MessageBox.Show(this, "Iniciado!", "Erro", MessageBoxButtons.OK);
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Erro", MessageBoxButtons.OK);
            }

            
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                var con = _sessionPw.Connect(txtServer.Text);
                MessageBox.Show(this, "Retorno = " + con, "Erro", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Erro", MessageBoxButtons.OK);
            }
            
        }

        private void btnGetScreen_Click(object sender, EventArgs e)
        {
            try
            {
                var txt = _sessionPw.GetScreenAt(1, 1, 1920);
                txtPw.Text = txt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Erro", MessageBoxButtons.OK);
            }
        }

        private void btnSplitScreen_Click(object sender, EventArgs e)
        {
            var texto = _sessionPw.GetScreenAt(1, 1, 1920);
            int textSize = texto.Length;
            int pos = 0;
            string text = "";
            if (textSize > 81)
            {
                while (textSize > 81)
                {
                    textSize -= 81;
                    text += texto.Substring(pos, 80) + "\n";
                    pos += 81;
                }
            }
            text += (textSize == 81) ? texto.Substring(pos, textSize - 1) : texto.Substring(pos, textSize);
            txtPw.Text = text;

        }

        private void btnGetStatus_Click(object sender, EventArgs e)
        {
            var status = _sessionPw.GetState();
            MessageBox.Show("Status:" + status);
        }



    }
}
